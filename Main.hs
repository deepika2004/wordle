module Main where
import System.Random 
import Data.List 


filterCorrectWords :: String -> String -> [String] -> [String]
filterCorrectWords genWord feedback  =  filter  (\word -> all (\(response, genLetter, letter) -> (response == 'G' && genLetter == letter) 
                                        || (response=='Y' && elem genLetter word && genLetter/=letter) 
                                            || (response=='B' && notElem genLetter word)) $ zip3 feedback genWord word)

wordle' :: [String] -> String -> String -> Int -> IO ()
wordle' wordsList word response chances 
    | any (`notElem` "GYB") response = do
                putStrLn "Enter Correct response in G Y B only"
                wordle wordsList chances
    | otherwise = do
        if all (=='G') response then putStrLn "I won" 
        else do 
            putStrLn("You have " ++ show (chances-1) ++ " chances more")
            wordle filteredList (chances-1)
            where filteredList = filterCorrectWords word response wordsList

wordle :: [String] -> Int -> IO ()
wordle wordsList chances
    | chances == 0 = putStrLn "I Lost"
    | null wordsList = putStrLn "No words in the list"
    | otherwise = do
        randomNum <- randomRIO (0,length wordsList -1)
        let word = wordsList !! randomNum
        putStrLn word
        putStrLn "Enter your response as G Y B"
        response <- getLine
        wordle' wordsList word response chances


main :: IO ()
main = do
    wordsFile <- readFile "words.txt"
    let wordsList = lines wordsFile
    wordle wordsList 6 
    

